# Work rate irreversibly transferred to internal energy

**M.TANAKA**  
**2022.12.16**  

[TOC]

## 背景：流体中で生じる不可逆なエネルギー変換

流れの中の流体粒子（実際に粒子の形をしている訳ではない。ラグランジュ微分によって追跡される、膨大な数の分子を含み連続体と見なせるが、なおも微小であり数学的には点と見なせる領域）において、単位体積・単位時間あたりに起こるエネルギー変換（例：圧力から速度へ、速度から熱へ、等）を考える。圧縮性流体において、不可逆的に内部エネルギーに変換される（散逸する）エネルギーは次のように表される[^1]：

[^1]: Wu, J. Z., Ma, H. Y., & Zhou, M. D. (2015).  *Vortical Flows*. New York, NY: Springer. p25. 

```math
    \rho T \frac{Ds}{Dt} = \mathit{\Phi} - \nabla \cdot \bm{q} + \rho \dot{Q} . 
```

ここで、各文字の名称は次の通り。

| 文字            | 名称                    | SI単位                    |
|:---------------:|:------------------------|:-------------------------:|
| $\rho$          | 密度                    | $\mathrm{kg/m^3}$         |
| $T$             | 温度                    | $\mathrm{K}$              |
| $s$             | 比エントロピー           | $\mathrm{J/(kg \cdot K)}$ |
| $\mathit{\Phi}$ | 散逸率(dissipation rate) | $\mathrm{W/m^3}$          |
| $\bm{q}$        | 境界からの熱流束         | $\mathrm{W/m^2}$          |
| $\dot{Q}$       | 単位質量当たりの加熱     | $\mathrm{W/kg}$           |


## 動機：流体機械で生じるエネルギー散逸

ここでは、非圧縮かつ熱の授受のない系を考えたい。実用上そう見なせる流体機械は多いからである。

系が断熱の場合、先の式の右辺第二、第三項は消えて

```math
    \rho T \frac{Ds}{Dt} = \mathit{\Phi} . 
```

となる。ここで散逸率$\mathit{\Phi}$は、

```math
    \mathit{\Phi} \equiv 
        \mathbf{V} : \mathbf{D} .
```
```math
\begin{align}
    \mathbf{V} &\equiv 
        \lambda \vartheta \mathbf{I} + 2 \mu \mathbf{D} . \\
    \vartheta &\equiv
        \nabla \cdot \bm{u} . \\
    \mathbf{D} &\equiv 
        \frac{1}{2} \left[ \nabla \bm{u} + (\nabla \bm{u})^T \right] . \\
\end{align}
```
<!-- ```math
    \mathit{\Phi} \equiv 
        \mu_\theta \vartheta^2 
        + \mu \omega^2
        - 2 \mu \nabla \cdot ( \mathbf{B} \cdot \bm{u}) .  \\
    \vartheta \equiv
        \nabla \cdot \bm{u} . \\
    \mathbf{B} \equiv
        \vartheta \bm{I} - (\nabla \bm{u})^T .
``` -->

| 文字         | 名称                                | SI単位                |
|:------------:|:------------------------------------|:---------------------:|
| $\mathbf{V}$ | 粘性応力テンソル                     | $\mathrm{Pa}$         |
| $\mathbf{D}$ | 歪み速度テンソル(strain-rate tensor) | $\mathrm{1/s}$        |
| $\mu$        | 剪断粘性(shear viscosity)           | $\mathrm{Pa \cdot s}$ |
| $\lambda$    | 第二粘性(second viscosity)          | $\mathrm{Pa \cdot s}$ |
| $\bm{u}$     | 速度                                | $\mathrm{m/s}$        |

で定義される[^2]。非圧縮性流れにおいては$\vartheta = 0$であるから、

[^2]: 同、p24.

```math
    \mathit{\Phi} = 
        2 \mu \mathbf{D} : \mathbf{D} .
```

<!-- ```math
    \mathit{\Phi} = 
        \mu \omega^2
        + 2 \mu \nabla \cdot [ (\nabla \bm{u})^T \cdot \bm{u}] .
```

あるいはエンストロフィ$\xi \equiv \omega^2/2$を用いて表せば、

```math
    \mathit{\Phi} = 
        2 \mu \{ \xi
        + \nabla \cdot [ (\nabla \bm{u})^T \cdot \bm{u} ] \} .
``` -->

となる。これが、非圧縮性断熱流れにおいて生じる、単位体積当たりのエネルギー散逸であると言える。この量を可視化することで、流体機械の効率を悪化させる流れの様子を分析するのに役立つのではないかと考えた。

### :warning: 実用上の注意

上式を用いると，表面近傍での（＝剥離のない境界層での）散逸率は次のようになる：

```math
    \mathit{\Phi_{boundary}}  \approx  \mu \left( \frac{\partial u_t}{\partial n} \right) ^2
```

| 文字  | 名称                     | SI単位         |
|:-----:|:------------------------|:--------------:|
| $u_t$ | 物体表面接線方向の速度成分  | $\mathrm{m/s}$ |
| $n$   | 物体表面法線方向           | $\mathrm{m}$   |

境界層における速度勾配は大きく，さらにその2乗であるため，物体表面近傍で散逸率は非常に大きな値をとる．理論上は，散逸率を体積分すれば，入口出口間での全圧×体積流量（より正確には流速を重みとした全圧の面積分）の差と等しくなるはずだが，これを数値解析で一致させようとすると非常に細かい境界層メッシュが必要になるだろう（ $y^+ \approx 1$ くらい？）．あるいは壁関数（Spalding則など）を基に積分する際のルールを修正するとか？

細かくないメッシュ密度で解析を行う場合は，境界層領域での散逸率分布を定量的に評価するのは避けた方が良いだろう．


## 目的

OpenFOAMのfunctionとして、散逸率$\mathit{\Phi}$を計算できるようにしたい。
そのためには、以下の演算の処理方法を調べる必要がある：

1. ベクトルの勾配（戻り値は2階テンソルになる）
2. テンソルの転置
3. テンソルのダブルドット積

また、以下の変数の取得方法を調べる必要がある：

1. 粘性係数$\mu$
2. 速度$\bm{u}$

これらの課題を解決することを本プロジェクトの目的とする。今後、散逸率$\mathit{\Phi}$以外の様々な場の量を後処理によって求める際にもノウハウが役立つものと思う。


## 対象とするケース

チュートリアルから、なるべく計算負荷が軽く、散逸率の分布が面白そうな（自明でなさそうな）ものを選びたい。今回は試みに[windAroundBuildings](https://develop.openfoam.com/Development/openfoam/-/tree/OpenFOAM-v2206/tutorials/incompressible/simpleFoam/windAroundBuildings)を選んだ。


## 得られた結果

![](/readme_figs/U.png)
速度絶対値$|\bm{u}|$の分布

![](/readme_figs/dissipationRate.png)
散逸率$\mathit{\Phi}$の分布

大体次のようなことが分かる。
- 境界層における強い剪断によって生じる散逸が大半を占める。
- その他には後流での剪断層における散逸が目立つ。


## チュートリアルに加えた変更

### 散逸率$\mathit{\Phi}$を出力するfunction objectの追加

[`coded`というfunction object](https://www.openfoam.com/documentation/guides/latest/api/classFoam_1_1functionObjects_1_1codedFunctionObject.html#details)を利用して、dissipationRateというスカラー場変数を出力する関数を[新たに作成した](/windAroundBuildings/system/dissipationRate)。

### transportPropertiesの追記

散逸率$\mathit{\Phi}$の計算のために動粘性係数$\nu$でなく粘性係数$\mu$が必要だったので追記した。

### Allrun-parallelの追記

ポテンシャル流れに対する$\mathit{\Phi}$を見てみたいので、
```
runParallel potentialFoam -writep
runParallel $(getApplication) -postProcess -func dissipationRate -latestTime
```
と追記した。

- :warning: ポテンシャル流れは前提として$\mu=0$なので、無理やり非零の粘性係数を当てはめるのはナンセンスではある。短時間の解析でざっと見積もれれば十分、という状況で使いたいので、`applyBoundaryLayer`をかませてからやれば多少はmake senceかも？？

### fvSolutionの追記

ポテンシャル流れの計算のために`system/fvSolution.solvers`に`Phi`のエントリを追加した。

### fvSchemesの追記

ポテンシャル流れの計算のために`system/fvSchemes.stream.divSchemes`に`div(div(phi,U)) Gauss linear;`を追記した。

### Allrun.preの追記

可視化のために`touch para.foam`と追記した。

### ParaView stateファイルの作成

可視化のために`slice.pvsm`ファイルを作成した。


## 動作の確認方法

このプロジェクトフォルダをダウンロード、展開して、
```
cd windAroundBuildings/
./Allrun-parallel
```
で計算実行。初期化するには
```
./Allclean
```


## 進捗チェックリスト

演算の処理方法：

1. [x] ベクトルの勾配（戻り値は2階テンソルになる）
    - [function objectとしてある](https://www.openfoam.com/documentation/guides/latest/doc/guide-fos-field-grad.html)。ベクトルの勾配も返せる。
    - 今回の場合は数式中に組み込む方法が別に必要。のちに`coded`で扱うが、`fvc::grad()`で実現できる。
2. [x] テンソルの転置
    - テンソル`A`に対して`T(A)`で得られた。
3. [x] テンソルのダブルドット積
    - `coded`で`A && B`と記述。ドット積は`A & B`。
    - [演算子の記法](https://www.openfoam.com/documentation/guides/latest/doc/openfoam-guide-expression-syntax.html#autotoc_md1329)

変数の取得方法：

1. [x] 粘性係数$\mu$
    - `IOdictionary`変数と`dimensionedScalar`変数を用いて取得。
2. [x] 速度$\bm{u}$
    - `coded`にて`const auto& U = mesh().lookupObject<volVectorField>("U");`のように取得。

## 作業メモ

### function object "coded"

#### テンソルの転置
- テンソル`A`に対して`A.T()`で[得られるようだ](https://www.openfoam.com/documentation/guides/latest/doc/openfoam-guide-expression-syntax.html#autotoc_md1334)。
- 以下のようにひずみ速度テンソルを`coded`で計算してみた。[ファイル全体はこちら](/windAroundBuildings/system/dissipationRate)。
    ```cpp
    D = 0.5 * (fvc::grad(U) + fvc::grad(U).T());
    ```
- しかし、メンバー関数(変数？)`T`は存在しないというエラーを得る。$\nabla \bm{u}$は転置が可能な2階テンソルであり問題ないと思うのだが…？
    ```
    error: ‘class Foam::tmp<Foam::GeometricField<Foam::Tensor<double>, Foam::fvPatchField, Foam::volMesh> >’ has no member named ‘T’
    ```
- [次のように書くことで、転置演算を実行できた](https://groups.google.com/g/openfoam/c/ysGkeGxt8DU)。
    ```cpp
    D = 0.5 * (fvc::grad(U) + T(fvc::grad(U)));
    ```

#### 粘性係数$\mu$の取得
- $\mu$を求めるには、動粘性係数$\nu$と密度$\rho$の情報を取得する必要がある。

- $\nu$は`transportProperties`ディクショナリに記述されている。次のようにして読み込むことができる。
    - [参考1: "dimensionedScalar"でリポジトリ検索して出てきた](https://develop.openfoam.com/Development/openfoam/-/blob/master/applications/solvers/DNS/dnsFoam/readTransportProperties.H)
    - [参考2: 【OpenFOAM】手法を変えて処理を追加⑤ : 設定項目を実行時に読み込む (IOdictionary)](https://zenn.dev/inabower/articles/b7bdf092ff8cf8)

    ```cpp
    IOdictionary transportProperties
    (
        IOobject
        (
            "transportProperties",
            mesh().time().constant(),
            mesh(),
            IOobject::MUST_READ_IF_MODIFIED,
            IOobject::NO_WRITE
        )
    );
    
    dimensionedScalar nu
    (
        "nu",
        dimViscosity,
        transportProperties
    );
    ```

- これでは$\nu$しか参照できないので、`transportProperties`に$\rho, \mu$を追記する。
    ```
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    transportModel  Newtonian;

    nu              1.5e-05;        
    ```
    ↓↓↓
    ```
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    transportModel  Newtonian;

    nu              1.5e-05;        
    rho             1.0;        
    mu              #eval "$rho * $nu";        
    ```

- :warning: 粘性係数と動粘性係数の次元について、[APIガイドではこう書いている](https://www.openfoam.com/documentation/guides/latest/api/dimensionSets_8C.html)：
    ```
    const dimensionSet 	dimViscosity (dimArea/dimTime) ⇠つまりm2/s
    const dimensionSet 	dimDynamicViscosity (dimDensity *dimViscosity) ⇠つまりPa・s
    ```

    - 一瞬「**逆では？？？**」と思ったが、日本語でいう動粘性係数$\nu = \mu / \rho$は、[英語では"Kinematic viscosity"と呼ぶようだ](https://en.wikipedia.org/wiki/Viscosity#Dynamic_viscosity)。

        |                                     | in english        | 日本語   |
        |:-----------------------------------:|:------------------|:--------|
        |$\mu \mathrm{[Pa \cdot s]}$          |Dynamic viscosity  |粘性係数  |
        |$\nu = \mu / \rho \mathrm{[m^2 / s]}$|Kinematic viscosity|動粘性係数|
    
    - 英語版Wiki情報では、普通、単に"Viscosity"とだけいえばDynamic viscosityを指すらしいが、OpenFOAMでは逆にKinematic viscosityのことを指しており、区別するために"dynamicViscosity"という名称を用いている（想像だが、開発初期の頃はKinematic viscosityしか必要なかったのではないか？）

    - 日本人は間違えやすそう。注意せねば。


## 参考

- [OpenFOAMライブラリでプログラムを書く　(5)関数オブジェクト](https://www.terrabyte.co.jp/discover/index.php/2021/01/18/post-1720/)
- [OpenFOAMリポジトリで"codeExecute"を検索した結果](https://develop.openfoam.com/search?project_id=47&search=codeExecute)
- [tutorialsより、"codeExecute"の例](https://develop.openfoam.com/Development/openfoam/-/blob/OpenFOAM-v2206/tutorials/verificationAndValidation/schemes/nonOrthogonalChannel/setups.orig/common/system/controlDict)
- [ユーザガイドより、OpenFOAM演算子の記法](https://www.openfoam.com/documentation/guides/latest/doc/openfoam-guide-expression-syntax.html#autotoc_md1329)
